﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MailAPI.Models
{
    public class MailRequest
    {
        public string To { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public bool IsHTML { get; set; }
    }
}