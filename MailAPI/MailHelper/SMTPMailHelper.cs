﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace MailAPI.MailHelper
{
    public class SMTPMailHelper
    {
        public string SendHtmlFormattedEmail(string recepientEmail, string subject, string body, bool IsHtml)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = IsHtml;
                mailMessage.To.Add(new MailAddress(recepientEmail));
                mailMessage.Priority = MailPriority.High;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                bool IsPasswordRequired = Convert.ToBoolean(ConfigurationManager.AppSettings["IsPasswordRequired"]);
                if (IsPasswordRequired)
                {
                    NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                }
                smtp.Credentials = NetworkCred;
                if(int.Parse(ConfigurationManager.AppSettings["DeliveryMethod"])==1)
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                }
                else if (int.Parse(ConfigurationManager.AppSettings["DeliveryMethod"]) == 2)
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
                    smtp.PickupDirectoryLocation = ConfigurationManager.AppSettings["PickupDirectory"];
                }
                else if (int.Parse(ConfigurationManager.AppSettings["DeliveryMethod"]) == 3)
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    smtp.PickupDirectoryLocation = ConfigurationManager.AppSettings["PickupDirectory"];
                }


                smtp.Send(mailMessage);
                return "Mail Successfully Sent";
            }
        }
    }
}
