﻿using MailAPI.MailHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MailAPI.Models;

namespace MailAPI.Controllers
{
    public class SendMailController : ApiController
    {
        public HttpResponseMessage Get(string To,string Body, string Subject, bool IsHTML=true)
        {
            HttpResponseMessage response;
            try
            {
                SMTPMailHelper helper = new SMTPMailHelper();
                string result = helper.SendHtmlFormattedEmail(To, Body, Subject, IsHTML);
             
                return this.Request.CreateResponse(HttpStatusCode.OK, result);
               
            }
            catch (Exception ex)
            {
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message + Environment.NewLine + ex.StackTrace);
                return response;

            }
        }

        public HttpResponseMessage Post([FromBody] MailRequest request)
        {
            HttpResponseMessage response;

            if(request == null)
            {
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError, "Request is empty");
                return response;
            }
            try
            {
                SMTPMailHelper helper = new SMTPMailHelper();
                string result = helper.SendHtmlFormattedEmail(request.To, request.Body, request.Subject, request.IsHTML);

                return this.Request.CreateResponse(HttpStatusCode.OK, result);
                //if (objLoc !=null)

                //else
                //    return this.Request.CreateResponse(HttpStatusCode.InternalServerError, "No Data from API");
            }
            catch (Exception ex)
            {
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message + Environment.NewLine + ex.StackTrace);
                return response;

            }
        }
    }
}
